#include <QUrl>
#include <QDebug>
#include <QApplication>
#include <QSettings>
#include <QFileInfo>
#include "qt_windows.h"

void copy_clipboard(const std::wstring& str)
{
    auto len = str.size();

    auto hdst = ::GlobalAlloc(GMEM_MOVEABLE | GMEM_DDESHARE, (len + 1) * sizeof(WCHAR));
    auto dst = static_cast<LPWSTR>(::GlobalLock(hdst));
    memcpy(dst, str.data(), len*sizeof(WCHAR));
    dst[len] = 0;
    ::GlobalUnlock(hdst);

    // Set clipboard data
    if (!::OpenClipboard(nullptr)) qDebug() << ::GetLastError();
    ::EmptyClipboard();
    if (!::SetClipboardData(CF_UNICODETEXT, hdst)) qDebug() << ::GetLastError();
    ::CloseClipboard();
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	a.setApplicationVersion(QStringLiteral(APP_VERSION));
	a.setApplicationName(QStringLiteral(TARGET_PRODUCT));
	a.setOrganizationName(QStringLiteral(TARGET_COMPANY));
	a.setOrganizationDomain(QStringLiteral("wolfgirl.org"));

	auto parsed_args = a.arguments();

	if(parsed_args.size() < 2) {
		return -1;
	}
	auto fileinfo = QFileInfo(parsed_args.at(1));
	if(!fileinfo.exists()) {
		return -2;
	}
	auto filepath = fileinfo.absoluteFilePath();

	QSettings st;

	const auto prefix = st.value(QStringLiteral("prefix")).toString();
	const auto size = st.beginReadArray(QStringLiteral("paths"));
	for(int i = 0; i < size; ++i) {
		st.setArrayIndex(i);

		auto path = st.value(QStringLiteral("path")).toString();
		auto webpath  = st.value(QStringLiteral("webpath")).toString();

		if(path.isEmpty() || webpath.isEmpty())
			continue;

		if(filepath.startsWith(path)) {
			filepath.remove(0,path.size());
			filepath.prepend(webpath);
			filepath.prepend(prefix);
			filepath = QUrl(filepath).toEncoded();
			filepath.replace('#', QStringLiteral("%23"));
			break;
		}
	}
	st.endArray();

	copy_clipboard(filepath.toStdWString());
	return 0;
}
