QT += core gui widgets

CONFIG += c++14

QMAKE_TARGET_COMPANY = catgirl
QMAKE_TARGET_PRODUCT = CopyPath

DEFINES +=                                           \
    APP_VERSION=\\\"$$VERSION\\\"                    \
    TARGET_PRODUCT=\\\"$$QMAKE_TARGET_PRODUCT\\\"    \
    TARGET_COMPANY=\\\"$$QMAKE_TARGET_COMPANY\\\"


TARGET = CopyPath

TEMPLATE = app

SOURCES += main.cpp
